<%@page import="com.entities.TblEmpleados"%>
ed<%-- 
    Document   : listado
    Created on : 13-10-2020, 17:46:47
    Author     : IMG_W10
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
  TblEmpleados empleado=(TblEmpleados)request.getAttribute("empleado");
 
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Editar Empleados Ciisa</title>
    </head>
    
<form  name="form" action="MantencionEmpleado" method="POST">
                    <div class="form-group">
                        <label for="rut">Rut</label>
                        <input  name="rut" value="<%= empleado.getRut() %>" class="form-control" required id="rut" >
                           </div>
                    <br>
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input  name="nombre" value="<%= empleado.getNombre()%>" class="form-control" required id="nombre"">
                     </div>       
                    <br>
                    <div class="form-group">
                        <label for="apellido">Apellido</label>
                         <input  name="apellido" value="<%= empleado.getApellido()%>" class="form-control" required id="apellido" >
                 
                      </div>      
                    <div class="form-group">
                        <label for="fecha_nac">Fecha Nacimiento</label>
                         <input  name="fecha_nac" value="<%= empleado.getFechaNac()%>" class="form-control" required id="fecha_nac" >
                 
                      </div> 
                    <div class="form-group">
                        <label for="sueldo">Sueldo</label>
                         <input  name="sueldo" value="<%= empleado.getSueldo()%>" class="form-control" required id="sueldo" >
                 
                      </div> 
                    <br>
                    <button type="submit" name="accion" value="editarempleado" class="btn btn-success">Editar Empleado</button>
                           <button type="submit" class="btn btn-success">Salir</button>
                </form>
    
</html>
