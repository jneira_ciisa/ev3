<%-- 
    Document   : listado
    Created on : 13-10-2020, 17:46:47
    Author     : IMG_W10
--%>

<%@page import="java.util.Iterator"%>
<%@page import="com.entities.TblEmpleados"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    List<TblEmpleados> usuarios = (List<TblEmpleados>) request.getAttribute("TblEmpleados");
    Iterator<TblEmpleados> itJugadores = usuarios.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Listado de Empleados Ciisa</title>
    </head>
    
        <form name="form" action="MantencionEmpleado" method="POST">  
   
            <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
                <table border="1">
                    <thead>
                    <th>Rut</th>
                    <th>nombre </th>
                    <th>Apellido </th>
                    <th>Fecha Nacimiento </th>
                    <th>Sueldo </th>
                    <th> </th>
                    </thead>
                    <tbody>
                        <%while (itJugadores.hasNext()) {
                       TblEmpleados empleado = itJugadores.next();%>
                        <tr>
                            <td><%= empleado.getRut()%></td>
                            <td><%= empleado.getNombre()%></td>
                            <td><%= empleado.getApellido()%></td>
                            <td><%= empleado.getFechaNac()%></td>
                            <td><%= empleado.getSueldo()%></td>

                            <td> <input type="radio" name="selecccion" value="<%= empleado.getRut()%>"> </td>
                        </tr>
                        <%}%>                
                    </tbody>           
                </table>
                <button type="submit" name="accion" value="editar" class="btn btn-lg btn-secondary" style="margin-top: 20px;margin-bottom: 50px;">Editar Empleado</button>
                <button type="submit" name="accion" value="eliminar" class="btn btn-lg btn-secondary" style="margin-top: 20px;margin-bottom: 50px;">Eliminar Empleado</button>
                <button type="submit" name="accion"  value="crear" class="btn btn-lg btn-secondary" style="margin-top: 20px;margin-bottom: 50px;">Crear Empleado</button>
                

        </form>
                    <%-- 
             <form name="form" action="/EV3-1.0-SNAPSHOT/api/listar" method="get">  
    <button type="submit" name="accion"  value="listar" class="btn btn-lg btn-secondary" style="margin-top: 20px;margin-bottom: 50px;">APÍ</button>
            </form>--%>
<button onclick="location.href='/api/listar'" type="button">
         API</button>
</html>
