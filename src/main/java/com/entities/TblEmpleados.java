/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author IMG_W10
 */
@Entity
@Table(name = "tbl_empleados")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TblEmpleados.findAll", query = "SELECT t FROM TblEmpleados t"),
    @NamedQuery(name = "TblEmpleados.findByRut", query = "SELECT t FROM TblEmpleados t WHERE t.rut = :rut"),
    @NamedQuery(name = "TblEmpleados.findByNombre", query = "SELECT t FROM TblEmpleados t WHERE t.nombre = :nombre"),
    @NamedQuery(name = "TblEmpleados.findByApellido", query = "SELECT t FROM TblEmpleados t WHERE t.apellido = :apellido"),
    @NamedQuery(name = "TblEmpleados.findByFechaNac", query = "SELECT t FROM TblEmpleados t WHERE t.fechaNac = :fechaNac"),
    @NamedQuery(name = "TblEmpleados.findBySueldo", query = "SELECT t FROM TblEmpleados t WHERE t.sueldo = :sueldo")})
public class TblEmpleados implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "rut")
    private String rut;
    @Size(max = 2147483647)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 2147483647)
    @Column(name = "apellido")
    private String apellido;
    @Size(max = 2147483647)
    @Column(name = "fecha_nac")
    private String fechaNac;
    @Size(max = 2147483647)
    @Column(name = "sueldo")
    private String sueldo;

    public TblEmpleados() {
    }

    public TblEmpleados(String rut) {
        this.rut = rut;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(String fechaNac) {
        this.fechaNac = fechaNac;
    }

    public String getSueldo() {
        return sueldo;
    }

    public void setSueldo(String sueldo) {
        this.sueldo = sueldo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rut != null ? rut.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TblEmpleados)) {
            return false;
        }
        TblEmpleados other = (TblEmpleados) object;
        if ((this.rut == null && other.rut != null) || (this.rut != null && !this.rut.equals(other.rut))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.entities.TblEmpleados[ rut=" + rut + " ]";
    }
    
}
