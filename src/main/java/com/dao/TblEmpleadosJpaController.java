/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dao;

import com.dao.exceptions.NonexistentEntityException;
import com.dao.exceptions.PreexistingEntityException;
import com.entities.TblEmpleados;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author IMG_W10
 */
public class TblEmpleadosJpaController implements Serializable {
private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");
    public TblEmpleadosJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public TblEmpleadosJpaController() {
   
    }
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TblEmpleados tblEmpleados) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(tblEmpleados);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findTblEmpleados(tblEmpleados.getRut()) != null) {
                throw new PreexistingEntityException("TblEmpleados " + tblEmpleados + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TblEmpleados tblEmpleados) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            tblEmpleados = em.merge(tblEmpleados);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = tblEmpleados.getRut();
                if (findTblEmpleados(id) == null) {
                    throw new NonexistentEntityException("The tblEmpleados with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TblEmpleados tblEmpleados;
            try {
                tblEmpleados = em.getReference(TblEmpleados.class, id);
                tblEmpleados.getRut();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The tblEmpleados with id " + id + " no longer exists.", enfe);
            }
            em.remove(tblEmpleados);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TblEmpleados> findTblEmpleadosEntities() {
        return findTblEmpleadosEntities(true, -1, -1);
    }

    public List<TblEmpleados> findTblEmpleadosEntities(int maxResults, int firstResult) {
        return findTblEmpleadosEntities(false, maxResults, firstResult);
    }

    private List<TblEmpleados> findTblEmpleadosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TblEmpleados.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TblEmpleados findTblEmpleados(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TblEmpleados.class, id);
        } finally {
            em.close();
        }
    }

    public int getTblEmpleadosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TblEmpleados> rt = cq.from(TblEmpleados.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
