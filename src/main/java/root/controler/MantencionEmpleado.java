/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.controler;

import com.dao.TblEmpleadosJpaController;
import com.dao.exceptions.NonexistentEntityException;
import com.entities.TblEmpleados;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author IMG_W10
 */
@WebServlet(name = "MantencionEmpleado", urlPatterns = {"/MantencionEmpleado"})
public class MantencionEmpleado extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MantencionEmpleado</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MantencionEmpleado at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String accion = request.getParameter("accion");
        if (accion.equals("crear")) {
            request.getRequestDispatcher("crear.jsp").forward(request, response);
        }
        if (accion.equals("crearempleado")) {
            String rut = request.getParameter("rut");
            String nombre = request.getParameter("nombre");
            String apellido = request.getParameter("apellido");
            String fecha_nac = request.getParameter("fecha_nac");
            String sueldo = request.getParameter("sueldo");

            System.out.println("rut" + rut);
            System.out.println("nombre" + nombre);
            System.out.println("nombre" + apellido);
            System.out.println("fecha_nac" + fecha_nac);
            System.out.println("sueldo" + sueldo);

            TblEmpleadosJpaController dao = new TblEmpleadosJpaController();

            TblEmpleados nuevoTblEmpleados = new TblEmpleados();
            nuevoTblEmpleados.setRut(rut);
            nuevoTblEmpleados.setNombre(nombre);
            nuevoTblEmpleados.setApellido(apellido);
            nuevoTblEmpleados.setFechaNac(fecha_nac);
            nuevoTblEmpleados.setSueldo(sueldo);
            try {
                dao.create(nuevoTblEmpleados);
            } catch (Exception ex) {
                System.out.println("Error al crear" + rut);
                Logger.getLogger(MantencionEmpleado.class.getName()).log(Level.SEVERE, null, ex);
            }

            List<TblEmpleados> TblEmpleados = dao.findTblEmpleadosEntities();

            request.setAttribute("TblEmpleados", TblEmpleados);
            request.getRequestDispatcher("listado.jsp").forward(request, response);
        }
        if (accion.equals("eliminar")) {
            TblEmpleadosJpaController dao = new TblEmpleadosJpaController();
            String rut = request.getParameter("selecccion");
            try {
                dao.destroy(rut);
            } catch (NonexistentEntityException ex) {
                System.out.println("Error al eliminar" + rut);
                Logger.getLogger(MantencionEmpleado.class.getName()).log(Level.SEVERE, null, ex);
            }
            List<TblEmpleados> TblEmpleados = dao.findTblEmpleadosEntities();

            request.setAttribute("TblEmpleados", TblEmpleados);
            request.getRequestDispatcher("listado.jsp").forward(request, response);
        }
        if (accion.equals("editar")) {
            TblEmpleadosJpaController dao = new TblEmpleadosJpaController();
            String rut = request.getParameter("selecccion");
            TblEmpleados empleado = dao.findTblEmpleados(rut);
            List<TblEmpleados> TblEmpleados = dao.findTblEmpleadosEntities();

            request.setAttribute("empleado", empleado);
            request.getRequestDispatcher("editar.jsp").forward(request, response);
        }

        if (accion.equals("editarempleado")) {
            String rut = request.getParameter("rut");
            String nombre = request.getParameter("nombre");
            String apellido = request.getParameter("apellido");
            String fecha_nac = request.getParameter("fecha_nac");
            String sueldo = request.getParameter("sueldo");
            System.out.println("rut" + rut);
            System.out.println("nombre" + nombre);
            System.out.println("nombre" + apellido);
            System.out.println("fecha_nac" + fecha_nac);
            System.out.println("sueldo" + sueldo);

            TblEmpleadosJpaController dao = new TblEmpleadosJpaController();
            TblEmpleados tblEmpleados = new TblEmpleados();
            tblEmpleados.setRut(rut);
            tblEmpleados.setNombre(nombre);
            tblEmpleados.setApellido(apellido);
            tblEmpleados.setFechaNac(fecha_nac);
            tblEmpleados.setSueldo(sueldo);
            try {
                dao.edit(tblEmpleados);
            } catch (Exception ex) {
                System.out.println("Error al editar " + rut);
                Logger.getLogger(MantencionEmpleado.class.getName()).log(Level.SEVERE, null, ex);
            }
            List<TblEmpleados> TblEmpleados = dao.findTblEmpleadosEntities();

            request.setAttribute("TblEmpleados", TblEmpleados);
            request.getRequestDispatcher("listado.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
