/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.services;

import com.dao.TblEmpleadosJpaController;
import com.entities.TblEmpleados;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Administrador
 */
@Path("/listar")
public class Listar {
        @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarTodo() {
        TblEmpleadosJpaController dao = new TblEmpleadosJpaController();
        List<TblEmpleados> lista = dao.findTblEmpleadosEntities();
        System.out.println("lista.size():" + lista.size());
        return Response.ok(200).entity(lista).build();

    }
}
